package es.codeurjc.mastercloudapps.p3.worker;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.codeurjc.mastercloudapps.p3.worker.models.TaskProgressEntity;
import es.codeurjc.mastercloudapps.p3.worker.repositories.TaskProgressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles(value = {"h2"})
class TaskManagerRabbitTest {

    private TaskManager taskManager;
    @Autowired
    private TaskProgressRepository taskProgressRepository;
    private UpperCaseTask upperCaseTask = new UpperCaseTestTask();
    private RabbitTemplate rabbitTemplate = new RabbitTestTemplate(new ConnectionFactory() {
        @Override
        public Connection createConnection() throws AmqpException {
            return null;
        }

        @Override
        public String getHost() {
            return null;
        }

        @Override
        public int getPort() {
            return 0;
        }

        @Override
        public String getVirtualHost() {
            return null;
        }

        @Override
        public String getUsername() {
            return null;
        }

        @Override
        public void addConnectionListener(ConnectionListener connectionListener) {

        }

        @Override
        public boolean removeConnectionListener(ConnectionListener connectionListener) {
            return false;
        }

        @Override
        public void clearConnectionListeners() {

        }
    });

    private ObjectMapper json = new ObjectMapper();

    @BeforeEach
    void setUp(){
        this.taskManager = new TaskManager(this.taskProgressRepository, this.rabbitTemplate, this.upperCaseTask);
    }

    @Test
    void shouldNotBeNull(){
        assertNotNull(this.taskManager);
    }

    @Test
    void whenReceivedShouldWriteTaskProgress() throws Exception {
        TaskManager.NewTask newTask = new TaskManager.NewTask();
        newTask.id = 1;
        newTask.text = "Test text";
        this.taskManager.received(json.writeValueAsString(newTask));

        TaskProgressEntity taskProgress = this.taskProgressRepository.findById(1L).get();

        assertEquals(taskProgress.getProgress(), 100L);
        assertEquals(taskProgress.getResult(), "TEST TEXT");
    }
}
