package es.codeurjc.mastercloudapps.p3.worker;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class RabbitTestTemplate extends RabbitTemplate {

    public RabbitTestTemplate(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    public void convertAndSend(String routingKey, Object object){

    }


}
