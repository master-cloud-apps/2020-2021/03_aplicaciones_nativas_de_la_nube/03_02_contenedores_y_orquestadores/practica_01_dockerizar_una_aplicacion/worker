package es.codeurjc.mastercloudapps.p3.worker.repositories;

import es.codeurjc.mastercloudapps.p3.worker.Application;
import es.codeurjc.mastercloudapps.p3.worker.models.TaskProgressEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles(value = {"h2"})
class TaskProgressRepositoryTest {

    @Autowired
    private TaskProgressRepository taskProgressRepository;

    @Test
    void shouldNotBeNull(){
        assertNotNull(this.taskProgressRepository);
    }

    @Test
    void shouldCreateTaskProgress(){
        TaskProgressEntity taskProgress = new TaskProgressEntity(1L, 0L, false, null);

        this.taskProgressRepository.save(taskProgress);

        TaskProgressEntity taskSaved = this.taskProgressRepository.findById(1L).get();
        assertEquals(taskSaved.getId(), 1L);
    }
}
