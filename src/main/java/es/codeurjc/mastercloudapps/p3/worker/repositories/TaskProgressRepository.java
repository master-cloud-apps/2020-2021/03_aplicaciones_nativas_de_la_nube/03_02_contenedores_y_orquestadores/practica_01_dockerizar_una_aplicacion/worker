package es.codeurjc.mastercloudapps.p3.worker.repositories;

import es.codeurjc.mastercloudapps.p3.worker.models.TaskProgressEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskProgressRepository extends JpaRepository<TaskProgressEntity, Long> {
}
