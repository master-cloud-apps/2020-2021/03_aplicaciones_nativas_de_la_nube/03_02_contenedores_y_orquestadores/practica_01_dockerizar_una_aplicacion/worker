package es.codeurjc.mastercloudapps.p3.worker.models;

import es.codeurjc.mastercloudapps.p3.worker.TaskManager;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class TaskProgressEntity {

    @Id
    private Long id;

    private Long progress;
    private Boolean completed;
    private String result;

    public TaskProgressEntity(TaskManager.TaskProgress taskProgress) {
        this.id = Long.valueOf(taskProgress.id);
        this.progress = Long.valueOf(taskProgress.progress);
        this.completed = taskProgress.completed;
        this.result = taskProgress.result;
    }
}
